package Modele;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.*;
import java.io.*;
import java.util.*;

public class Client extends Observable {

    private Socket clientSock;	//socket de connexion

    private PrintWriter os; //flux de sortie
    private BufferedInputStream is; //flux d'entrée

    private String etat = ""; //État actuel du client

    private String adr; //Addresse IP du serveur
    private int port; //Port du serveur

    //private int numConnexion; //Numéro de la connexion obtenue avec le serveur
    private String alias; //Alias de l'usager

    private VerifierClient vc; //Thread d'inpection du texte
    private String texte; //Texte recu du serveur

    public String getServeurTexte(){
        return texte;
    }

    public Client(String adr, String nom, int port) {
        this.adr = adr;
        this.port = port;
        alias = nom;
    }

    public Client(String adr, String nom) {
        this(adr, nom, 5555);
    }

    public Client(String nom) {
        this("127.0.0.1", nom, 5555);
    }

    public Client() {
        this("127.0.0.1", "", 5555);
    }

    private boolean estConnecte() {
        return clientSock != null; //ou utiliser etat != "DECONNECTE"
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String e) {
        etat = e;
        setChanged();
        notifyObservers();
    }

    public void setAlias(String a) {
        alias = a;
    }

    public String getAlias() {
        return alias;
    }

    public String getAdrServeur() {
        return adr;
    }

    public int getPortServeur() {
        return port;
    }

    public void setPortServeur(int p) {
        port = p;
        setChanged();
        notifyObservers();
    }

    public void setAdrServeur(String a) {
        adr = a;
        setChanged();
        notifyObservers();
    }

    public void envoyer(int code, String message) {

        System.out.println(message);
        os.print(code + "|" + alias + ">>" + message);
        os.flush();

    }

    public int connecter() {
        int code = 500;

        if (!estConnecte()) {
            try {
                setEtat("RECH_SERVEUR");

                //Connecter au serveur
                clientSock = new Socket(adr, port); //On suppose que le serveur utilise le port 5555.

                is = new BufferedInputStream(clientSock.getInputStream());
                os = new PrintWriter(clientSock.getOutputStream());

                setEtat("CONNECTION");

                String msg = this.lireMessage();
                if (msg.startsWith("200")) {
                    code = 200;
                    //numConnexion = 1;
                    setEtat("CONNECTÉ");
                    this.envoyer(200, alias + " a rejoint la salle de chat");
                    this.setAlias(alias);

                    //Démarrer l'inspecteur
                    vc = new VerifierClient(this);
                    vc.start();

                } else {
                    code = 403;
                    clientSock.close();
                    clientSock = null;
                    setEtat("DECONNECTE");

                }

            } catch (IOException e) {
                System.out.println("\nNO SERVER...");
            }
        }else{

        }
        return code;
    }

    public boolean deconnecter() {
        if (!estConnecte()) {
            return true;
        }
        try {
            envoyer(150, alias + " a quitté la salle de chat");
            vc.interrupt();
            clientSock.close();
            clientSock = null;
            setEtat("DECONNECTE");
            return true;
        } catch (IOException io) {
            return false;
        }
    }

    public String lireMessage() {

        byte[] buf = new byte[500];	//buffer de lecture
        String msg;				//Texte lu

        try {
            is.read(buf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        msg = (new String(buf)).trim();
        System.out.println(msg);


        return msg;
    }

    public void lire() {
        texte = this.lireMessage();
        setChanged();
        notifyObservers(texte);

    }
}

class VerifierClient extends Thread {

    Client ref;

    public VerifierClient(Client cli) {
        ref = cli;
    }

    public void run() {
        while (!interrupted()) {
            //Lire le socket
            ref.lire();
            try {
                //Laisser une chance d'exécution aux autres threads
                Thread.sleep(1000);
            } catch (Exception x) {
                System.out.println("Exception dans thread" + x);
            }
        }
    }
}