
import Modele.Client;

import org.junit.jupiter.api.*;
import Serveur.Serveur;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class ServeurIntegrationTest {


    public static int port;
//    public static Client c1;
//    public static Client c2;
//    public static Client c3;
//    public static Client c4;
//    public static Client c5;
//    public static Client c6;
    public static Serveur ser;

    @BeforeAll
    public static void init() throws InterruptedException, IOException {

        // Prendre un port disponible
        ServerSocket s = new ServerSocket(0);
        port = s.getLocalPort();
        s.close();

        ser = new Serveur(port);

        Executors.newSingleThreadExecutor()
                .submit(() -> ser.demarrer());
        TimeUnit.SECONDS.sleep(2);
    }


    @Test
    void TestA_Salon_de_jeu_Logique_commande_principale_salon() throws InterruptedException {
        String escompte_1 = "FIFA (5)" + "\nFortnite (5)" + "\nDiablo (5)";
        String escompte_2 = "Vous avez été ajouté au salon FIFA";
        String escompte_3 = "Vous avez été déconnecté du salon FIFA";
        String escompte_4 = "Vous êtes déja conneté à un salon";

        //Afficher la liste des salons sans rejoindre
        Client c1 = new Client("127.0.0.1","joueur 1",port);
        c1.connecter();
        TimeUnit.SECONDS.sleep(1);
        c1.envoyer(100,"list rooms");
        TimeUnit.SECONDS.sleep(1);
        String reel_1 = c1.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);

        //Rejoindre un salon
        c1.envoyer(100,"join room FIFA");
        TimeUnit.SECONDS.sleep(1);
        String reel_2 = c1.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);

        //Rejoindre un salon
        c1.envoyer(100,"join room FIFA");
        TimeUnit.SECONDS.sleep(1);
        String reel_4 = c1.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);

        //Quitter un salon
        c1.envoyer(100,"quit room");
        TimeUnit.SECONDS.sleep(1);
        String reel_3 = c1.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);

        assertEquals(escompte_1, reel_1);
        assertEquals(escompte_2, reel_2);
        assertEquals(escompte_4, reel_4);
        assertEquals(escompte_3, reel_3);

    }

    @Test
    void TestB_Salon_de_jeu_logique_salon_plein() throws InterruptedException {
        String escompte = "FIFA (5)" + "\nFortnite (5)" + "\nDiablo (3)";
        String escompte_1 = "Le salon Diablo est comblé ...";

        Client x1 = new Client("127.0.0.1","X1",port);
        x1.connecter();
        TimeUnit.SECONDS.sleep(1);
        x1.envoyer(100,"join room Diablo");
        TimeUnit.SECONDS.sleep(1);

        Client x2 = new Client("127.0.0.1","X1",port);
        x2.connecter();
        TimeUnit.SECONDS.sleep(1);
        x2.envoyer(100,"join room Diablo");
        TimeUnit.SECONDS.sleep(1);

        x1.envoyer(100,"list rooms");
        TimeUnit.SECONDS.sleep(1);
        String reel = x1.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);

        Client x3 = new Client("127.0.0.1","X1",port);
        x3.connecter();
        TimeUnit.SECONDS.sleep(1);
        x3.envoyer(100,"join room Diablo");
        TimeUnit.SECONDS.sleep(1);

        Client x4 = new Client("127.0.0.1","X1",port);
        x4.connecter();
        TimeUnit.SECONDS.sleep(1);
        x4.envoyer(100,"join room Diablo");
        TimeUnit.SECONDS.sleep(1);

        Client x5 = new Client("127.0.0.1","X1",port);
        x5.connecter();
        TimeUnit.SECONDS.sleep(1);
        x5.envoyer(100,"join room Diablo");
        TimeUnit.SECONDS.sleep(1);

        Client x6 = new Client("127.0.0.1","X1",port);
        x6.connecter();
        TimeUnit.SECONDS.sleep(1);
        x6.envoyer(100,"join room Diablo");
        TimeUnit.SECONDS.sleep(1);
        String reel_1 = x6.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);


        assertEquals(escompte, reel);
        assertEquals(escompte_1, reel_1);

    }


    @Test
    void TestC_lancement_jeu_scenario_1_si_joueur_pas_inscrit() throws InterruptedException {

        String escompte_1 = "Vous n'êtes connecté à aucun salon";

        //Le joueur 1 commence un jeu sans rejoindre un salon
        Client y1 = new Client("127.0.0.1","joueur 1",port);
        y1.connecter();
        TimeUnit.SECONDS.sleep(1);
        y1.envoyer(400,"begin game");
        TimeUnit.SECONDS.sleep(3);
        String reel_1 = y1.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);
        assertEquals(escompte_1, reel_1);
    }

    @Test
    void TestD_lancement_jeu_scenario_2_si_un_seul_joueur_present() throws InterruptedException {
        String escompte_1 = "Le salon doit avoir au moins 2 joueurs pour lancer un jeu";

        Client z1 = new Client("127.0.0.1","joueur 1",port);
        z1.connecter();
        TimeUnit.SECONDS.sleep(1);
        z1.envoyer(100,"join room Fortnite");
        TimeUnit.SECONDS.sleep(1);
        z1.envoyer(400,"begin game");
        TimeUnit.SECONDS.sleep(3);
        String reel_1 = z1.getServeurTexte();
        TimeUnit.SECONDS.sleep(1);
        assertEquals(escompte_1, reel_1);
    }

    @Test
    void TestE_lancement_jeu_scenario_3_flow_normal_de_jeu() throws InterruptedException {
        String escompte_2 = "Une invitation est envoyée aux autres joueurs!\n" +
                "En attente de confirmation des autres joueurs";

        Client b1 = new Client("127.0.0.1","joueur 1",port);
        b1.connecter();
        TimeUnit.SECONDS.sleep(1);
        b1.envoyer(100,"join room Fortnite");
        TimeUnit.SECONDS.sleep(1);

        Client d2 = new Client("127.0.0.1","joueur 2",port);
        d2.connecter();
        TimeUnit.SECONDS.sleep(1);
        d2.envoyer(100,"join room Fortnite");
        TimeUnit.SECONDS.sleep(1);

        Client d3 = new Client("127.0.0.1","joueur 3",port);
        d3.connecter();
        TimeUnit.SECONDS.sleep(1);
        d3.envoyer(100,"join room Fortnite");
        TimeUnit.SECONDS.sleep(1);

        b1.envoyer(400,"begin game");
        TimeUnit.SECONDS.sleep(3);


        d2.envoyer(800,"accept game");
        TimeUnit.SECONDS.sleep(3);


        d3.envoyer(800,"accept game");
        TimeUnit.SECONDS.sleep(3);

        String reel_2 = b1.getServeurTexte();
        TimeUnit.SECONDS.sleep(5);

        assertEquals(escompte_2, reel_2);
    }
}


