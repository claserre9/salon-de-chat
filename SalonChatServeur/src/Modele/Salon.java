package Modele;

import Serveur.Connexion;

import java.util.ArrayList;

public class Salon {
    private String nomSalon;
    private final int LIMIT_USERS = 5;
    private int compteur;
    private ArrayList<Connexion> listConnexion = new ArrayList<>();
    //Verifier si le jeu est lancé dans le salon
    private boolean isLauched = false;
    // Confirme que tous les joueurs ont accepté de jouer
    private boolean isCurrentlyPlaying = false;

    public Salon(String nom){
        this.nomSalon = nom;
    }


    public String getNomSalon() {
        return nomSalon;
    }

    public void ajouterConnexion(Connexion c){
        if (isNotFull()){
            listConnexion.add(c);
            compteur++;
        }
        else throw new IllegalStateException("Le salon "+ this.getNomSalon()+ " est comblé ...");

    }
    public void quitConnexion(Connexion c){
            listConnexion.remove(c);
            compteur--;
    }

    public boolean isBusy(){
        for (Connexion c: listConnexion){
            if (c.isPlaying()){
                continue;
            }else{
                return false;
            }
        }
        return true;
    }

    public boolean isNotFull(){
        return compteur < LIMIT_USERS;
    }

    public ArrayList<Connexion> getListConnexion(){
        return listConnexion;
    }

    public int nombreDePlacesRestantes(){
        return LIMIT_USERS - compteur;
    }

    public boolean isLauched() {
        return isLauched;
    }

    public void setLauched(boolean lauched) {
        isLauched = lauched;
    }

    public boolean isCurrentlyPlaying() {
        return isCurrentlyPlaying;
    }

    public void setCurrentlyPlaying(boolean currentlyPlaying) {
        isCurrentlyPlaying = currentlyPlaying;
    }

}
