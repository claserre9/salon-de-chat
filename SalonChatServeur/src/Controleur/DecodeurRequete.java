package Controleur;

public class DecodeurRequete {
    public static final String separateur = ">>";

    public static String decode(String texte){
        return texte.substring(texte.indexOf(">>") + separateur.length());
    }
}
