package Controleur;

import Modele.Salon;
import Serveur.Connexion;
import Serveur.Serveur;

import java.util.function.Consumer;

public class SalonController {
    private String texte;
    private Connexion client;
    private Serveur serveur;

    //Les méthodes étant des actions

    public SalonController(String texte, Connexion client, Serveur serveur ){
        this.texte = texte;
        this.client = client;
        this.serveur = serveur;
    }



    public void afficherListeSalon(String requete){
        StringBuilder buffer = new StringBuilder();
        if(requete.equals("list rooms")){
            serveur.getListSalon()
                    .forEach(salon -> buffer.append("\n"+salon.getNomSalon() +
                            " ("+salon.nombreDePlacesRestantes()+")"));
        }
        client.envoyerMessage(buffer.toString());

    }

    public void rejoindreSalon(String requete){
        if (requete.split(" ")[0].equals("join") && requete.split(" ")[1].equals("room")){
            if (requete.split(" ").length == 3){
                for (Salon s : serveur.getListSalon()){
                    if(requete.split(" ")[2].equals(s.getNomSalon())){
                        if (!client.isConnectedToRoom()){
                            try{
                                s.ajouterConnexion(client);
                                client.envoyerMessage("\n"+"Vous avez été ajouté au salon " + s.getNomSalon());
                                client.setConnectedToRoom(true);
                                client.setAppartenance(s.getNomSalon());
                                break;
                            }catch (IllegalStateException e){
                                client.envoyerMessage(e.getMessage());
                            }
                        }else{
                            client.envoyerMessage("Vous êtes déja conneté à un salon");
                        }
                    }
                }
            }
        }

    }

    public void quitterSalon(String requete){
        if (requete.split(" ")[0].equals("quit") && requete.split(" ")[1].equals("room")){
            for (Salon s :serveur.getListSalon()){
                if (client.isConnectedToRoom()){
                    s.quitConnexion(client);
                    client.envoyerMessage("\n"+"Vous avez été déconnecté du salon " + s.getNomSalon());
                    client.setConnectedToRoom(false);
                    client.setAppartenance("");
                }else{
                    client.envoyerMessage("Vous n'êtes connecté à aucun salon");
                }
                break;
            }
        }

    }

    public void envoyerMessageDansSalon(String requete){
        if(serveur.nomSalonToList().contains(requete.split(" ",2)[0])){
            if(client.getAppartenance().equals(requete.split(" ",2)[0])){
                for (Connexion receveur : serveur.getConnexions()){
                    if(receveur.getAppartenance().equals(requete.split(" ",2)[0])){
                        receveur.envoyerMessage("("+client.getAlias()+") =>" + requete.split(" ",2)[1]);
                    }
                }
            }else{
                client.envoyerMessage("Vous ne faites pas partie de ce salon !");
            }
        }

    }

    public void invoke() {
        if (texte.startsWith("100")){
            //On decode la requête
            String requete = DecodeurRequete.decode(texte);

            //Action pour afficher la liste de salon
            afficherListeSalon(requete);

            //Action pour rejoindre un salon
            rejoindreSalon(requete);

            //Action pour quitter un salon
            quitterSalon(requete);

            //Action pour envoyer un message aux membres d'un salon
            envoyerMessageDansSalon(requete);
        }
    }
}
