package Controleur;

import Serveur.Connexion;
import Serveur.Serveur;

public class UserController {
    private Connexion client;
    private String texte;

    public UserController(String texte, Connexion client) {
        this.texte = texte;
        this.client = client;
    }

    public void login(){
        client.setAlias(texte.substring(texte.indexOf("|")+1).split(">>")[0]);
        //client.envoyerMessage("Bonjour " + client.getAlias());
    }


    public void invoke(){
        if(texte.startsWith("200")) login();
    }
}
