package Controleur;

import Modele.Salon;
import Serveur.Connexion;
import Serveur.Serveur;
import Serveur.VerifierCritereJeu;


public class GameController {
    private Connexion client;
    private Serveur serveur;
    private String texte;


    public GameController(String texte, Connexion client, Serveur serveur) {
        this.texte = texte;
        this.client = client;
        this.serveur = serveur;
    }


    public void verifierJeu(){
        VerifierCritereJeu vc = new VerifierCritereJeu(serveur, client);
        //Renvoie l'exception lancée du Thread "Serveur.VerifierCritereJeu" vers le Thread "principal"
        vc.setUncaughtExceptionHandler((t, e) -> client.envoyerMessage(e.getMessage()));
        if(serveur.FindSalon(client.getAppartenance())!=null
                && serveur.FindSalon(client.getAppartenance()).isLauched()){
            client.envoyerMessage("Le jeu est déja lancé dans le salon "+ client.getAppartenance());
        }else {
            //client.envoyerMessage("Aucun jeu n'a été lancé / Personne n'a lancé de jeu ...");
            vc.start();
        }

    }


    public void lancerJeu(){
        Salon s = serveur.FindSalon(client.getAppartenance());
        if (!s.isCurrentlyPlaying()) {
            if(!client.isPlaying()) {
                client.envoyerMessage("\nEn attente d'autres joueurs");
                client.setPlaying(true);
            }else{
                client.envoyerMessage("\nVous avez déjà lancé / accepté l'invitation");
            }
            if (s.isBusy()) {
                for (Connexion c : serveur.getConnexions()){
                    if(s.getNomSalon().equals(c.getAppartenance())){
                        c.envoyerMessage("\nTous le joueurs ont accepté de jouer!");
                        c.envoyerMessage("\nDebut de la partie...!");
                    }
                }
                s.setCurrentlyPlaying(true);
                //La jeu peut officiellement commencer à partir de cette ligne
                //TODO
            }
        }else{
            client.envoyerMessage("Impossible d'executer cette commande dans votre cas");
        }
    }

    public void invoke() {
        if (texte.startsWith("800")){
            lancerJeu();
        }

        if(texte.startsWith("400")){
            verifierJeu();
        }
    }
}
