package Serveur;

import Modele.Salon;

import java.util.ArrayList;

//Thread de création de 3 salons
class CreerSalon extends Thread {

    Serveur ref;

    public CreerSalon(Serveur cs) {
        ref = cs;
    }

    public void run(){
        ArrayList<Salon> listSalon = ref.getListSalon();
        listSalon.add(new Salon("FIFA"));
        listSalon.add(new Salon("Fortnite"));
        listSalon.add(new Salon("Diablo"));

    }
}
