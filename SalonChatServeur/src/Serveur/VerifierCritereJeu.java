package Serveur;

import Modele.Salon;

public class VerifierCritereJeu extends Thread {
    Serveur ref;
    Connexion exp;


    public VerifierCritereJeu(Serveur cs, Connexion ex){
        ref = cs;
        exp = ex;
    }

    public void run() throws IllegalStateException {
        try {
            exp.envoyerMessage("Vérification des critères sur le joueur...");
            Thread.sleep(1000);
            if(!exp.isConnectedToRoom()){
                //exp.envoyerMessage("\nVous n'êtes connecté à aucun salon");
                throw new IllegalStateException("\nVous n'êtes connecté à aucun salon");

            }else{
                exp.envoyerMessage("Vérification des critères sur le salon "+ exp.getAppartenance());
                Thread.sleep(1000);
                if(ref.nombreJoueurDansUnSalon(exp.getAppartenance()) < 2){
                    //exp.envoyerMessage("\nLe salon doit avoir au moins 2 joueurs pour lancer un jeu");
                    throw new IllegalStateException("\nLe salon doit avoir au moins 2 joueurs pour lancer un jeu");
                }

            }

        } catch (InterruptedException e) {
            //Si le thread est interrompu pour une raison inconnue
            e.printStackTrace();
        }

        exp.envoyerMessage("\nUne invitation est envoyée aux autres joueurs!");
        exp.envoyerMessage("\nEn attente de confirmation des autres joueurs");
        exp.setPlaying(true);//Le joueur a la statut de joueur
        exp.setLauchedGame(true);
        //On confirme que la vérification a été faite sans lancer d'exception
        ref.FindSalon(exp.getAppartenance()).setLauched(true);
        for (Salon s : ref.getListSalon()){
            if(s.getNomSalon().equals(exp.getAppartenance())){
                for (Connexion c : s.getListConnexion()){
                    if(c.getAppartenance().equals(exp.getAppartenance()) && !c.getAlias().equals(exp.getAlias())){
                        c.setInvited(true);
                        c.envoyerMessage("\n"+ exp.getAlias() + " vous invite à jouer dans " + s.getNomSalon());
                        c.envoyerMessage("\nTaper 'accept game' pour confirmer et appuyer sur 'Enter'");
                    }
                }
            }
        }
    }
}
