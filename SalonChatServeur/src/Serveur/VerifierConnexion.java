package Serveur;

// Thread d'inspection des connections
class VerifierConnexion extends Thread {

    Serveur ref;

    public VerifierConnexion(Serveur cs) {
        ref = cs;
    }

    public void run() {
        while (true) {
            ref.attente();
            try {
                //Laisser une chance d'exécution aux autres threads
                Thread.sleep(10);
            } catch (Exception ignored) {
            }
        }
    }
}
