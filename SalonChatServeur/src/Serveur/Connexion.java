package Serveur;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Modele.Salon;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Connexion {

    private Socket interne;

    private PrintWriter os; //Les flux de sortie
    private BufferedInputStream is; //Les flux d'entrée
    private String alias;

    private boolean isConnectedToRoom = false;
    private String stampOfSalon ="";

    //Indice qui montre que le joueur joue dans le salon qu'il a rejoint
    private boolean isPlaying = false;

    //Confirme que le joueur a recu une invitation
    private boolean isInvited = false;

    //Confirme que le joueur a lance des invitations aux autres joueurs
    private boolean lauchedGame = false;

    private ArrayList<Salon> listSalon = new ArrayList<>();

    public Connexion(ServerSocket serveurSock) throws IOException {
        interne = serveurSock.accept();
        os = new PrintWriter(interne.getOutputStream());
        is = new BufferedInputStream(interne.getInputStream());

    }

    public Socket getInterne() {
        return interne;
    }

    public int getPort() {
        return interne.getPort();
    }

    public PrintWriter getOs() {
        return os;
    }

    public BufferedInputStream getIs() {
        return is;
    }

    public void envoyerMessage(String msg) {
        if (os != null) {
            os.print(msg);
            os.flush();
        }
    }

    public boolean messageEnAttente() throws IOException {

        return is.available() > 0;
    }

    public String lireMessage() throws IOException {
        String texte = null;

        if (is != null) {
            //buffer de lecture
            byte buf[] = new byte[500];

            is.read(buf);
            texte = (new String(buf)).trim();

            //Effacer le buffer
            buf = null;
        }
        return texte;
    }

    public void closeConnexion() throws IOException {
        os.close();
        is.close();
        interne.close();

        os = null;
        is = null;
        interne = null;

    }

    //Cette methode permet d'injecter la liste des salons du serveur à une connection
    //Donc, par extension au client
    public void giveListSalon(ArrayList<Salon> listSalon) {
        this.listSalon = listSalon;
    }

    //Permet au client d'acceder à la liste de salon
    public ArrayList<Salon> AccessToListSalon(){
        return this.listSalon;
    }

    public boolean isConnectedToRoom() {
        return isConnectedToRoom;
    }

    public void setConnectedToRoom(boolean connectedToRoom) {
        isConnectedToRoom = connectedToRoom;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setAppartenance(String stampOfSalon) {
        this.stampOfSalon = stampOfSalon;
    }

    public String getAppartenance(){ return this.stampOfSalon; }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }

    public boolean isLauchedGame() {
        return lauchedGame;
    }

    public void setLauchedGame(boolean lauchedGame) {
        this.lauchedGame = lauchedGame;
    }
}
