package Serveur;

import Controleur.GameController;
import Controleur.SalonController;
import Controleur.UserController;
import Modele.Salon;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Serveur {

    private ServerSocket serveurSock;
    private int port;

    private final int maxConnexions = 15;

    public ArrayList<Connexion> getConnexions() {
        return connexions;
    }

    private ArrayList<Connexion> connexions = new ArrayList<>();
    private ArrayList<Salon> listSalon = new ArrayList<>();

    private VerifierCommunication vt; //Thread d'inspection du serveur

    private Lock verrou = new ReentrantLock();


    public Serveur(int p) {
        port = p;
    }

    public Serveur() {
        this(5555);
    }

    public ArrayList<Salon> getListSalon() {
        return listSalon;
    }

    public ArrayList<String> nomSalonToList(){
        ArrayList<String> arraySalon = new ArrayList<>();
        for (Salon s : this.listSalon){
            arraySalon.add(s.getNomSalon());
        }
        return arraySalon;
    }

    //Methode permettant de fournir le nombre de joueurs inscrits dans un salon
    public int nombreJoueurDansUnSalon(String nomSalon){
        int compteurdejoueurs = 0;
        for(Salon s : this.listSalon){
            if(s.getNomSalon().equals(nomSalon)){
                for (Connexion c : s.getListConnexion()){
                    if(c.getAppartenance().equals(nomSalon)){
                        compteurdejoueurs++;
                    }
                }
            }
        }
       return compteurdejoueurs ;
    }

    public Connexion obtenirConnexion(String alias){
        for(Connexion c: connexions){
            if(c.getAlias().equals(alias)) return c;
        }
        return null;
    }

    public Salon FindSalon(String nomSalon){
        for (Salon s : this.listSalon){
            if (s.getNomSalon().equals(nomSalon)){
                return s;
            }
        }
        return null;
    }

    //Connecter comme Serveur.Serveur
    public void demarrer() {
        try {

            System.out.println("Démarrage du serveur...");
            Thread.sleep(1000);
            //Création du socket
            serveurSock = new ServerSocket(port);

            //Démarrer l'inspecteur
            VerifierConnexion vc = new VerifierConnexion(this);
            vc.start();


            System.out.println("Chargement des salons");
            CreerSalon cs = new CreerSalon(this);
            cs.start();


            //Message à l'usager
            Thread.sleep(1000);
            System.out.println("Serveur.Serveur à l'ecoute sur le port " + serveurSock.getLocalPort());

        } catch (IOException | InterruptedException e) {
            System.out.println("\nServeur.Serveur déjà actif sur ce port...");
        }
    }

    //==================================================
    //Attente d'une connection
    //==================================================
    public void attente() {
        try {

            //Attente d'une connection :
            Connexion client = new Connexion(serveurSock);
            client.giveListSalon(this.listSalon);

            // Première connection?
            if(connexions.size() ==0) {
                //Oui, démarrer le thread inspecteur de texte :
                vt = new VerifierCommunication(this);
                vt.start();
            }

            // Place disponible dans la chatroom
            if (connexions.size() < maxConnexions) {
                //Envoyer le code 200 pour confirmer la connexion
                client.envoyerMessage("200 Bienvenue dans le chat");

                //Mémorisation de la connection
                verrou.lock();
                try {
                    connexions.add(client);
                } finally {
                    verrou.unlock();
                }

                //Message à l'usager
                System.out.println("Serveur.Connexion sur le port " + client.getPort());
            } else {
                //Envoyer le code 403 pour refuser la connexion
                client.envoyerMessage("403 Désolé la salle de chat est pleine");
                //Message à l'usager
                System.out.println("Serveur.Connexion sur le port #" + client.getPort() + " refusée. La salle de chat est pleine");
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    //==================================================
    //Lire le socket
    //==================================================
    public void lire() {
        verrou.lock();
        try {

            ArrayList<Connexion> fermees = new ArrayList<>();
            String texte;

            //Lire toutes les connections
            for (Connexion expediteur : connexions) {
                //La connection est-elle active?
                if (expediteur.messageEnAttente()) {
                    //Oui, lire le socket
                    texte = expediteur.lireMessage();

                    //Controlleur utilisateur
                    new UserController(texte, expediteur).invoke();

                    //Controlleur Jeu
                    new GameController(texte,expediteur, this).invoke();

                    //Controleur Salon
                    new SalonController(texte, expediteur, this).invoke();

                    LoginAndLogoutBroadcast(texte, expediteur);

                    if (texte.startsWith("150")) {
                        try {
                            expediteur.closeConnexion();
                            fermees.add(expediteur);
                            System.out.println("TODO : Deconnecter " + texte);
                        } catch (Exception x) {
                            x.printStackTrace();
                        }
                    }
                }
            }
            // on efface les connexions fermées à la fin afin de ne pas avoir
            // d'exception due à une opération concurente
            // On ne peut pas parcourir et modifier un arrayList dans la même boucle
            fermerConnexions(fermees);

        } catch (IOException ignored) {
        } finally {
            verrou.unlock();
        }
    }

    //Broadcast à tout le monde sauf l'expediteur
    private void LoginAndLogoutBroadcast(String texte, Connexion expediteur) {
        for (Connexion receveur : connexions) {
            //Ne pas renvoyer le message à l'expéditeur
            if (receveur != expediteur) {
                if (texte.startsWith("200")) {
                    receveur.envoyerMessage(texte.substring(texte.indexOf(">>")));
                }else if(texte.startsWith("150")){
                    receveur.envoyerMessage(texte.substring(texte.indexOf(">>")));
                }
            }
        }
    }

    private void fermerConnexions(ArrayList<Connexion> fermetures) {
        connexions.removeAll(fermetures);
        if (connexions.size() == 0) { // arrêter le thread s'il n'y a plus de client
            vt.interrupt();
        }
    }


    //main() démarre le serveur:
    public static void main(String[] args) {
        Serveur serveur = new Serveur();
        serveur.demarrer();
    }


}

