package Serveur;

// Thread d'inspection d'arrivé de texte:
class VerifierCommunication extends Thread {

    Serveur ref;

    public VerifierCommunication(Serveur cs) {
        ref = cs;
    }

    public void run() {
        while (!interrupted()) {
            ref.lire();
            try {
                //Laisser une chance d'exécution aux autres threads
                Thread.sleep(10);
            } catch (Exception ignored) {
            }
        }
    }
}
