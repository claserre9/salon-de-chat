**Développement d’un client et serveur de salons de
jeu utilisant les sockets**

Ce projet est un programme bonifié d'un premier programme de salon de chat 
Client-Serveur.

L'objectif du projet est d’ajouter des fonctionnalités nécessaires au démarrage
d'un jeu dans le salon de chat entre plusieurs utilisateurs (Les détails de ces
fonctionnalités sont décrites en tant que recit utilisateur - *voir les [Issues 
du projet](https://gitlab.com/claserre9/salon-de-chat/-/issues?scope=all&utf8=%E2%9C%93&state=all&milestone_title=Sprint%201)
*)

La première fonctionnalité est de créer un salon de jeu afin que des joueurs se
rencontrent pour commencer une partie. La deuxième est de permettre à un joueur 
d'initialiser une partie.

Voici la liste des protocoles permettant à l'utilisateur de jouir des fonctionnalités
du programme. Ces protocoles sont saisis à clavier à partir de l'interface utilisateur
du programme.

*  list rooms (Affiche la liste des salons disponibles et le nombre de places disponibles)
*  join room <nom du salon> (Permet au joueur de rejoindre un salon)
*  quit room (Permet de quitter un salon)
*  <nom du salon> <message> (Permet de diffuser un message à tous les joueurs présents dans un salon)
*  begin game (Permet de lancer un jeu dans le salon sous forme d'invitation)
*  accept game (Permet d'accepter une invitation à un jeu)





